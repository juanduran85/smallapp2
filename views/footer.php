<footer>

</footer>
<script src="js/bootstrap.min.js" type="text/javascript"></script>
<script src="js/bootstrap.bundle.min.js" type="text/javascript"></script>
<script src="js/jquery-3.3.1.min.js" type="text/javascript"></script>
<script src="js/validate.js" type="text/javascript"></script>

<script>
$(document).ready(function(){
  var current = 1;
  var current_step,next_step,steps;
  
  steps = $("fieldset").length;

  console.log(steps);
  
  $(".next").click(function(){
    current_step = $(this).parent();
    next_step = $(this).parent().next();
    next_step.show();
    current_step.hide();
    setProgressBar(++current);
  });

  $(".previous").click(function(){
    current_step = $(this).parent();
    next_step = $(this).parent().prev();
    next_step.show();
    current_step.hide();
    setProgressBar(--current);
  });

  setProgressBar(current);

  function setProgressBar(curStep){
    if (curStep == 1) {
      var percent = 0;
      $(".progress-bar")
      .css("width",percent+"%")
      .html(percent+"%");
    } else if (curStep == 2) {
      var percent = 50;
      $(".progress-bar")
      .css("width",percent+"%")
      .html(percent+"%");
    }else if (curStep == 3) {
      var percent = 100;
      $(".progress-bar")
      .css("width",percent+"%")
      .html(percent+"%");
    } 
  }
});
</script>
</body>
</html>