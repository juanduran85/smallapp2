function validate()
{
	var form1=document.form1;
	
	if (form1.firstname.value==0)
	{
		swal(
		  'Oops...',
		  'Something is wrong, Please enter a first name!',
		  'warning'
		)
		form1.firstname.value="";
		form1.firstname.focus();
		return false;
	}

	if (validate_string(form1.firstname.value)==false)
	{
		swal(
		  'Oops...',
		  'Something is wrong, Please enter a valid first name!',
		  'error'
		)
		form1.firstname.value="";
		form1.firstname.focus();
		return false;
	}	

	if (form1.lastname.value==0)
	{
		swal(
		  'Oops...',
		  'Something is wrong, Please enter a last name!',
		  'warning'
		)
		form1.lastname.value="";
		form1.lastname.focus();
		return false;
	}

	if (validate_string(form1.lastname.value)==false)
	{
		swal(
		  'Oops...',
		  'Something is wrong, Please enter a valid last name!',
		  'error'
		)
		form1.lastname.value="";
		form1.lastname.focus();
		return false;
	}	
	
	if (form1.telephone.value==0)
	{
		swal(
		  'Oops...',
		  'Something is wrong, Please enter a telephone!',
		  'warning'
		)		
		form1.telephone.value="";
		form1.telephone.focus();
		return false;
	}
	
	if (validate_number(form1.telephone.value)==false)
	{
		swal(
		  'Oops...',
		  'Something is wrong, Please enter a valid telephone!',
		  'error'
		)		
		form1.telephone.value="";
		form1.telephone.focus();
		return false;
	}	

	document.form1.submit();
}

function clean()
{
	document.form1.reset();
	document.form1.firstname.focus();
}

function validate_number(number1)
{
	if (!/^([0-9])*$/.test(number1))
	{
		return (false);
	}
	else
	{
		return (true);
	}
}

function validate_string(text1)
{
	var RegExPattern = "[1-20]";

	if (text1.match(RegExPattern))
	{
		return (false);
	}
	else
	{
		return (true);
	}
}