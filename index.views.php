<?php
require_once("functions.php");
require_once("views/header.php"); 

if (isset($_POST['firstname']) && isset($_POST['lastname']) && isset($_POST['telephone']) && isset($_POST['address']) && isset($_POST['housenumber']) && isset($_POST['zipcode']) && isset($_POST['city']) && isset($_POST['accountowner']) && isset($_POST['iban'])) {

  $firstname = clean_text($_POST['firstname']);
  $lastname = clean_text($_POST['lastname']);
  $telephone = clean_text($_POST['telephone']);
  $address = clean_text($_POST['address']);
  $housenumber = clean_text($_POST['housenumber']);
  $zipcode = clean_text($_POST['zipcode']);
  $city = clean_text($_POST['city']);
  $accountowner = clean_text($_POST['accountowner']);
  $iban = clean_text($_POST['iban']);

  $url = 'https://37f32cl571.execute-api.eu-central-1.amazonaws.com/default/wunderfleet-recruiting-backend-dev-save-payment-data';
   
  $ch = curl_init($url);
   
  $jsonData = array(
      'customerId' => 1,
      'iban'       => $iban,
      'owner'      => $accountowner
  );
   
  $jsonDataEncoded = json_encode($jsonData);
  curl_setopt($ch, CURLOPT_POST, 1);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);
  curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
  $result = curl_exec($ch);
  $result2 = json_decode($result);
  foreach ($result2 as $key) {
    $paymentDataId = $key;
  }

  connection_db();

  $insertousers = $connection->prepare('INSERT INTO users (firstname,lastname,telephone,address,housenumber,zipcode,city,accountowner,iban,paymentDataId) VALUES ( :firstname, :lastname, :telephone, :address, :housenumber, :zipcode, :city, :accountowner, :iban, :paymentDataId)');
  $insertousers->execute(array(
      ':firstname'=>$firstname,
      ':lastname'=>$lastname,
      ':telephone'=>$telephone,
      ':address'=>$address,
      ':housenumber'=>$housenumber,
      ':zipcode'=>$zipcode,
      ':city'=>$city,
      ':accountowner'=>$accountowner,
      ':iban'=>$iban,
      ':paymentDataId'=>$paymentDataId
  ));

echo "<script  type='text/javascript'>
      swal({
        title: 'Good...',
        text: 'Registered user successfully. PaymentDataId: $paymentDataId',
        type: 'success',
        showCancelButton: false,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ok!'
      }).then((result) => {
        if (result.value) {
        swal(
          'I get it',
          'Returning to the entry form',
          'success'
        )
        }
      window.location='http://localhost/small_app/';  
      })</script>";
}
require_once("views/footer.php");
?>