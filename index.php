<?php 
require_once("views/header.php"); 
?>
  <form id="regiration_form" action="index.views.php" method="POST" name="form1">
  <fieldset>
    <h2>Step 1: Insert personal information</h2>
  <div class="form-row">
    <div class="col-md-4 mb-3">
      <label for="firstname">First name</label>
      <input type="text" name="firstname" class="form-control" id="firstname" placeholder="First Name" required autofocus="autofocus" maxlength="15" pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚüÜ]{2,25}+[a-zA-ZñÑáéíóúÁÉÍÓÚüÜ]{2,25}+[a-zA-ZñÑáéíóúÁÉÍÓÚüÜ]{2,25}">
    </div>
  </div>
  <div class="form-row">
    <div class="col-md-4 mb-3">
      <label for="lastname">Last name</label>
      <input type="text" name="lastname" class="form-control" id="lastname" placeholder="Last name" required maxlength="15" pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚüÜ]{2,25}+[a-zA-ZñÑáéíóúÁÉÍÓÚüÜ]{2,25}+[a-zA-ZñÑáéíóúÁÉÍÓÚüÜ]{2,25}">
    </div>
  </div>  
  <div class="form-row">
    <div class="col-md-4 mb-3">
      <label for="telephone">Telephone</label>
      <input type="tel" name="telephone" class="form-control" id="telephone" placeholder="Telephone" required maxlength="11" pattern="[0-9]{11}">
    </div>
  </div>
    <input type="button" class="next btn btn-outline-info" value="Next" />
  </fieldset>
  <fieldset>
    <h2>Step 2: Insert Address Information</h2>
  <div class="form-row">
    <div class="col-md-4 mb-3">
      <label for="address">Address</label>
      <input type="text" name="address" class="form-control" id="address" placeholder="Address Including Street" required>
    </div>
  </div>
  <div class="form-row">
    <div class="col-md-4 mb-3">
      <label for="housenumber">House Number</label>
      <input type="text" name="housenumber" class="form-control" id="housenumber" placeholder="House Numbere" required>
    </div>
  </div>  
  <div class="form-row">
    <div class="col-md-4 mb-3">
      <label for="zipcode">Zip Code</label>
      <input type="text" name="zipcode" class="form-control" id="zipcode" placeholder="Zip Code" required>
    </div>
  </div>
  <div class="form-row">
    <div class="col-md-4 mb-3">
      <label for="city">City</label>
      <input type="text" name="city" class="form-control" id="city" placeholder="City" required maxlength="20" pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚüÜ]{2,25}+[a-zA-ZñÑáéíóúÁÉÍÓÚüÜ]{2,25}+[a-zA-ZñÑáéíóúÁÉÍÓÚüÜ]{2,25}">
    </div>
  </div>  
    <input type="button" name="previous" class="previous btn btn-outline-secondary" value="Previous" />
    <input type="button" name="next" class="next btn btn-outline-info" value="Next" />
  </fieldset>
  <fieldset>
    <h2>Step 3: Insert Payment Information</h2>
  <div class="form-row">
    <div class="col-md-4 mb-3">
      <label for="accountowner">Account owner</label>
      <input type="text" name="accountowner" class="form-control" id="accountowner" placeholder="Account owner" required>
    </div>
  </div>
  <div class="form-row">
    <div class="col-md-4 mb-3">
      <label for="iban">IBAN</label>
      <input type="text" name="iban" class="form-control" id="iban" placeholder="IBAN" required>
    </div>
  </div>  
    <input type="button" name="previous" class="previous btn btn-outline-secondary" value="Previous" />
    <input type="submit" name="submit" class="submit btn btn-outline-success" id="sub1" value="Submit" onclick="validate()"/>
  </fieldset>
  </form>

  <div class="progress st1">
    <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
  </div>

<?php 
require_once("views/footer.php");
?>